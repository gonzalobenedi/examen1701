@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dishes</div>

                <div class="panel-body">
                    <a href="/dishes/create">Crear un plato</a>
                    <table class="table">
                        <thead>
                            <th>Nombre</th>
                            <th>Tipo</th>
                            <th>Nombre Ususario</th>
                            <th>Detalles</th>
                        </thead>
                        @foreach ($dishes as $dish)
                            <tr>
                                <td>{{$dish->name}}</td>
                                <td>{{$dish->type->name}}</td>
                                <td>{{$dish->user->name}}</td>
                                <td><a href="/dishes/{{$dish->id}}">Ver más...</a></td>

                                @can('delete', $dish)
                                <td>
                                    <form class="" action="/dishes/{{$dish->id}}" method="post">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="submit" value="Borrar" class="btn btn-danger">
                                    </form>
                                </td>

                                @endcan
                            </tr>
                        @endforeach
                    </table>
                    {{ $dishes->render() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
