@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dishes</div>

                <div class="panel-body">
                    <h4>Detalles del plato</h4>
                    <hr>
                    <p><b>Nombre:</b> {{$dish->name}}</p>
                    <p><b>Tipo:</b> {{$dish->type->name}}</p>
                    <p><b>Usuario:</b> {{$dish->user->name}}</p>
                    <p><b>Descripción:</b> {{$dish->description}}</p>
                    <hr>
                    <h4>Ingredientes</h4>
                    <table class="table">
                        <thead>
                            <th>Nombre</th>
                            <th>Cantidad</th>
                        </thead>
                        @foreach ($dish->ingredients as $ingredient)
                            <tr>
                                <td>{{$ingredient->name}}</td>
                                <td>{{$ingredient->pivot->quantity}}</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
